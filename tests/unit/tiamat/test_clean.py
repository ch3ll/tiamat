from unittest import mock


def test_remove_dist(hub, tmpdir):
    with mock.patch("shutil.rmtree") as rm:
        hub.tiamat.clean.remove_dist(tmpdir)
        rm.assert_called_once()


def test_remove_build(hub, tmpdir):
    with mock.patch("shutil.rmtree") as rm:
        hub.tiamat.clean.remove_build(tmpdir)
        rm.assert_called_once()


def test_remove_pytest_cache(hub, tmpdir):
    with mock.patch("shutil.rmtree") as rm:
        hub.tiamat.clean.remove_pytest_cache(tmpdir)
        rm.assert_called_once()


def test_remove_logs(hub, tmpdir):
    tmpdir.join("log.foo").write("")  # This shouldn't be deleted

    log_file = tmpdir.join("foo.log")
    log_file.write("")
    with mock.patch("os.remove") as rm:
        hub.tiamat.clean.remove_logs(tmpdir)
        rm.assert_called_once_with(str(log_file))
