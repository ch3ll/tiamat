def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.pyvenv.bin = hub.tiamat.virtualenv.pyvenv.bin
    mock_hub.tiamat.virtualenv.pyvenv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.pyvenv.create = hub.tiamat.virtualenv.pyvenv.create
    mock_hub.tiamat.virtualenv.pyvenv.create(bname)
