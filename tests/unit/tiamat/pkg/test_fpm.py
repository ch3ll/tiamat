from unittest import mock


def test_build(hub, mock_hub, bname):
    mock_hub.tiamat.pkg.fpm.build = hub.tiamat.pkg.fpm.build
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].onedir = False
    mock_hub.tiamat.BUILDS[bname].name = "test_name"
    mock_hub.tiamat.BUILDS[bname].pkg = {}
    mock_hub.tiamat.BUILDS[bname].pkg_tgt = "pacman"
    mock_hub.tiamat.BUILDS[bname].dir = "test_dir"
    mock_hub.tiamat.BUILDS[bname].dependencies = "test_deps"
    mock_hub.tiamat.BUILDS[bname].release = "test_release"
    mock_hub.tiamat.BUILDS[bname].version = "1"
    mock_data = ""
    mock_stat = mock.MagicMock()
    mock_stat.st_mode = 1

    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(retcode=0)

    with mock.patch("os.makedirs"), mock.patch(
        "os.stat", return_value=mock_stat
    ), mock.patch("shutil.copy"), mock.patch(
        "builtins.open", mock.mock_open(read_data=mock_data)
    ), mock.patch(
        "shutil.which"
    ):
        mock_hub.tiamat.pkg.fpm.build(bname)
