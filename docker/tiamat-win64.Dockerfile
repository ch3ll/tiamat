FROM cdrx/pyinstaller-windows:latest
SHELL ["/bin/bash", "-i", "-c"]

# upgrade pip
RUN /usr/bin/python -m pip install --upgrade pip wheel

# Install tiamat
ADD ./ /tmp/tiamat-src
RUN /usr/bin/pip install /tmp/tiamat-src
RUN echo 'wine '\''C:\Python37\Scripts\tiamat.exe'\'' "$@"' > /usr/bin/tiamat
RUN chmod +x /usr/bin/tiamat

COPY docker/entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
